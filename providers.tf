terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=3.74.0, <5"
    }
  }

  required_version = ">= 1.2"
}

provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
  token      = var.aws_session_token
}