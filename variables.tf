variable "vpc_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "region" {}
variable "subnet_cidr_block" {}

variable "access_key" {
  description = "Access key to AWS console"
}
variable "secret_key" {
  description = "Secret key to AWS console"
}

variable "aws_session_token" {
  description = "Temporary session token used to create instances"
}

variable "my_ip" {
  description = "My public ip"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "public_key_location" {}